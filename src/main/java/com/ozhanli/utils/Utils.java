package com.ozhanli.utils;

import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class Utils {

    private static Properties properties = new Properties();
    private static Logger log = Logger.getLogger(Utils.class);

    static {
        String configFile = System.getProperty("application.properties");
        if (configFile == null) {
            configFile = "application.properties";
        }
        loadConfig(configFile);

    }

    private static void loadConfig(String filename) {
        if (filename == null) {
            log.warn("Config filename cannot be null");
        } else {
            try {
                log.info("Loading config file: " + filename);
                final InputStream fis = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
                properties.load(fis);
            } catch (FileNotFoundException fne) {
                log.error("filename not found " + filename, fne);
            } catch (IOException ioe) {
                log.error("Cannot read  " + filename, ioe);
            }
        }
    }

    public static String getStringProperty(String key) {
        String value = properties.getProperty(key);
        if (value == null) {
            value = System.getProperty(key);
        }
        return value;
    }


}
