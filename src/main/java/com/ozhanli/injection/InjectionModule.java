package com.ozhanli.injection;

import com.google.inject.AbstractModule;
import com.ozhanli.service.*;

// TODO: Guice module implementation
public class InjectionModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(UserService.class).to(UserServiceImpl.class);
        bind(AccountService.class).to(AccountServiceImpl.class);
        bind(TransactionService.class).to(TransactionServiceImpl.class);
    }
}