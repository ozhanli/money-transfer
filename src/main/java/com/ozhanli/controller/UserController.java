package com.ozhanli.controller;

import com.ozhanli.exception.CustomException;
import com.ozhanli.model.User;
import com.ozhanli.model.base.BaseResponse;
import com.ozhanli.service.factory.ServiceFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class UserController {

    private final ServiceFactory serviceFactory = ServiceFactory.getServiceFactory(0);

    @GET
    @Path("users")
    public List<User> getAllUsers() throws CustomException {
        return serviceFactory.getUserService().getAllUsers();
    }

    @GET
    @Path("users/{username}")
    public BaseResponse getByUsername(@PathParam("username") String username) throws CustomException {
        return serviceFactory.getUserService().getByUsername(username);
    }

    @POST
    @Path("users")
    public BaseResponse createUser(User user) throws CustomException {
        return serviceFactory.getUserService().create(user);
    }

    @PUT
    @Path("users/{userId}")
    public BaseResponse update(@PathParam("userId") long userId, User user) throws CustomException {
        return serviceFactory.getUserService().update(userId, user);
    }

    @DELETE
    @Path("users/{userId}")
    public BaseResponse delete(@PathParam("userId") long userId) throws CustomException {
        return serviceFactory.getUserService().delete(userId);
    }

}
