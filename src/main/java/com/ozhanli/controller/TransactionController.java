package com.ozhanli.controller;

import com.ozhanli.exception.CustomException;
import com.ozhanli.model.Transaction;
import com.ozhanli.model.base.BaseResponse;
import com.ozhanli.service.factory.ServiceFactory;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionController {

    private final ServiceFactory serviceFactory = ServiceFactory.getServiceFactory(0);

    @POST
    @Path("transactions")
    public BaseResponse transfer(Transaction transaction) throws CustomException {
        return serviceFactory.getTransactionService().transfer(transaction);
    }

}
