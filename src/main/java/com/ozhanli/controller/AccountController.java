package com.ozhanli.controller;

import com.ozhanli.exception.CustomException;
import com.ozhanli.model.Account;
import com.ozhanli.model.base.BaseResponse;
import com.ozhanli.service.factory.ServiceFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.math.BigDecimal;

@Path("/")
@Produces(MediaType.APPLICATION_JSON)
public class AccountController {

    private final ServiceFactory serviceFactory = ServiceFactory.getServiceFactory(0);

    @GET
    @Path("accounts")
    public BaseResponse getAllAccounts() throws CustomException {
        return serviceFactory.getAccountService().getAccounts();
    }

    @GET
    @Path("accounts/{accountId}")
    public BaseResponse getAccountById(@PathParam("accountId") Long accountId) throws CustomException {
        return serviceFactory.getAccountService().getAccountById(accountId);
    }

    @GET
    @Path("accounts/{accountId}/balance")
    public BaseResponse getAccountBalanceById(@PathParam("accountId") Long accountId) throws CustomException {
        return serviceFactory.getAccountService().getAccountBalanceById(accountId);
    }

    @POST
    @Path("accounts")
    public BaseResponse createAccount(Account account) throws CustomException {
        return serviceFactory.getAccountService().createAccount(account);
    }

    @PUT
    @Path("accounts/{accountId}/deposit/{amount}")
    public BaseResponse depositToAccount(@PathParam("accountId") Long accountId,
                                         @PathParam("amount") BigDecimal amount) throws CustomException {
        return serviceFactory.getAccountService().depositToAccount(accountId, amount);
    }

    @PUT
    @Path("accounts/{accountId}/withdraw/{amount}")
    public BaseResponse withdrawFromAccount(@PathParam("accountId") Long accountId,
                                            @PathParam("amount") BigDecimal amount) throws CustomException {
        return serviceFactory.getAccountService().withdrawFromAccount(accountId, amount);
    }

    @DELETE
    @Path("accounts/{accountId}")
    public BaseResponse deleteAccountById(@PathParam("accountId") Long accountId) throws CustomException {
        return serviceFactory.getAccountService().deleteAccountById(accountId);
    }

}
