package com.ozhanli.repository;

import com.ozhanli.exception.CustomException;
import com.ozhanli.model.User;
import com.ozhanli.repository.factory.H2RepositoryFactory;
import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class UserRepositoryImpl implements UserRepository {

    private static Logger log = Logger.getLogger(UserRepositoryImpl.class);
    private final static String SQL_GET_USER_BY_ID = "SELECT * FROM User WHERE id = ? ";
    private final static String SQL_GET_ALL_USERS = "SELECT * FROM User";
    private final static String SQL_GET_USER_BY_NAME = "SELECT * FROM User WHERE username = ? ";
    private final static String SQL_INSERT_USER = "INSERT INTO User (username, email) VALUES (?, ?)";
    private final static String SQL_UPDATE_USER = "UPDATE User SET username = ?, email = ? WHERE id = ? ";
    private final static String SQL_DELETE_USER_BY_ID = "DELETE FROM User WHERE id = ? ";


    public List<User> getAllUsers() throws CustomException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<User> users = new ArrayList<User>();
        try {
            conn = H2RepositoryFactory.getConnection();
            stmt = conn.prepareStatement(SQL_GET_ALL_USERS);
            rs = stmt.executeQuery();
            while (rs.next()) {
                User u = new User(rs.getLong("id"), rs.getString("username"), rs.getString("email"));
                users.add(u);
            }
            return users;
        } catch (SQLException e) {
            throw new CustomException("Cannot read user data");
        } finally {
            DbUtils.closeQuietly(conn, stmt, rs);
        }
    }

    public Optional<User> getById(long userId) throws CustomException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        User user = null;
        try {
            conn = H2RepositoryFactory.getConnection();
            stmt = conn.prepareStatement(SQL_GET_USER_BY_ID);
            stmt.setLong(1, userId);
            rs = stmt.executeQuery();
            if (rs.next()) {
                user = new User(rs.getLong("id"), rs.getString("username"), rs.getString("email"));
            }
            return Optional.ofNullable(user);
        } catch (SQLException e) {
            throw new CustomException("Cannot read user data");
        } finally {
            DbUtils.closeQuietly(conn, stmt, rs);
        }
    }

    public Optional<User> getByUsername(String userName) throws CustomException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        User u = null;
        try {
            conn = H2RepositoryFactory.getConnection();
            stmt = conn.prepareStatement(SQL_GET_USER_BY_NAME);
            stmt.setString(1, userName);
            rs = stmt.executeQuery();
            if (rs.next()) {
                u = new User(rs.getLong("id"), rs.getString("username"), rs.getString("email"));
                if (log.isDebugEnabled())
                    log.debug("Retrieve User: " + u);
            }
            return Optional.ofNullable(u);
        } catch (SQLException e) {
            throw new CustomException("");
        } finally {
            DbUtils.closeQuietly(conn, stmt, rs);
        }
    }

    public long insert(User user) throws CustomException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet generatedKeys = null;
        try {
            conn = H2RepositoryFactory.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, user.getUsername());
            stmt.setString(2, user.getEmail());
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new CustomException("User create error for account: " + user.getUsername());
            }
            generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            } else {
                throw new CustomException("User create error for account: " + user.getUsername());
            }
        } catch (SQLException e) {
            throw new CustomException("User create error for account: " + user.getUsername());
        } finally {
            DbUtils.closeQuietly(conn, stmt, generatedKeys);
        }

    }

    public int update(Long userId, User user) throws CustomException {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = H2RepositoryFactory.getConnection();
            stmt = conn.prepareStatement(SQL_UPDATE_USER);
            stmt.setString(1, user.getUsername());
            stmt.setString(2, user.getEmail());
            stmt.setLong(3, userId);
            return stmt.executeUpdate();
        } catch (SQLException e) {
            throw new CustomException("User update error for account: " + userId);
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(stmt);
        }
    }

    public int delete(long userId) throws CustomException {
        Connection conn = null;
        PreparedStatement stmt = null;

        try {
            conn = H2RepositoryFactory.getConnection();
            stmt = conn.prepareStatement(SQL_DELETE_USER_BY_ID);
            stmt.setLong(1, userId);
            return stmt.executeUpdate();
        } catch (SQLException e) {
            throw new CustomException("User delete error for account: " + userId);
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(stmt);
        }
    }

}
