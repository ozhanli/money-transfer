package com.ozhanli.repository;

import com.ozhanli.exception.CustomException;
import com.ozhanli.model.Account;
import com.ozhanli.model.Transaction;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;


public interface AccountRepository {

    List<Account> getAllAccounts() throws CustomException;

    Optional<Account> getAccountById(long accountId) throws CustomException;

    long createAccount(Account account) throws CustomException;

    int deleteAccountById(long accountId) throws CustomException;

    int updateAccountBalance(long accountId, BigDecimal deltaAmount) throws CustomException;

    int transferAccountBalance(Transaction transaction) throws CustomException;
}
