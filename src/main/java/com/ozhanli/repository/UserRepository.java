package com.ozhanli.repository;

import com.ozhanli.exception.CustomException;
import com.ozhanli.model.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    List<User> getAllUsers() throws CustomException;

    Optional<User> getById(long userId) throws CustomException;

    Optional<User> getByUsername(String username) throws CustomException;

    long insert(User user) throws CustomException;

    int update(Long userId, User user) throws CustomException;

    int delete(long userId) throws CustomException;

}
