package com.ozhanli.repository;

import com.ozhanli.enums.MoneyEnum;
import com.ozhanli.exception.CustomException;
import com.ozhanli.model.Account;
import com.ozhanli.model.Transaction;
import com.ozhanli.repository.factory.H2RepositoryFactory;
import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AccountRepositoryImpl implements AccountRepository {

    private static Logger log = Logger.getLogger(AccountRepositoryImpl.class);
    private final static String SQL_GET_ACC_BY_ID = "SELECT * FROM Account WHERE id = ? ";
    private final static String SQL_LOCK_ACC_BY_ID = "SELECT * FROM Account WHERE id = ? FOR UPDATE";
    private final static String SQL_CREATE_ACC = "INSERT INTO Account (username, balance, currencyCode) VALUES (?, ?, ?)";
    private final static String SQL_UPDATE_ACC_BALANCE = "UPDATE Account SET balance = ? WHERE id = ? ";
    private final static String SQL_GET_ALL_ACC = "SELECT * FROM Account";
    private final static String SQL_DELETE_ACC_BY_ID = "DELETE FROM Account WHERE id = ?";

    public List<Account> getAllAccounts() throws CustomException {
        Connection conn = null;
        PreparedStatement statement = null;
        ResultSet rs = null;
        List<Account> accounts = new ArrayList<>();
        try {
            conn = H2RepositoryFactory.getConnection();
            statement = conn.prepareStatement(SQL_GET_ALL_ACC);
            rs = statement.executeQuery();
            while (rs.next()) {
                Account acc = new Account(rs.getLong("id"), rs.getString("username"),
                        rs.getBigDecimal("balance"), rs.getString("currencyCode"));
                accounts.add(acc);
            }
            return accounts;
        } catch (SQLException e) {
            throw new CustomException("Cannot read account data");
        } finally {
            DbUtils.closeQuietly(conn, statement, rs);
        }
    }

    public Optional<Account> getAccountById(long accountId) throws CustomException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Account account = null;
        try {
            conn = H2RepositoryFactory.getConnection();
            stmt = conn.prepareStatement(SQL_GET_ACC_BY_ID);
            stmt.setLong(1, accountId);
            rs = stmt.executeQuery();
            if (rs.next()) {
                account = new Account(rs.getLong("id"), rs.getString("username"),
                        rs.getBigDecimal("balance"), rs.getString("currencyCode"));
                if (log.isDebugEnabled())
                    log.debug("Retrieve Account By Id: " + account);
            }
            return Optional.ofNullable(account);
        } catch (SQLException e) {
            throw new CustomException("Cannot read account data");
        } finally {
            DbUtils.closeQuietly(conn, stmt, rs);
        }

    }

    public long createAccount(Account account) throws CustomException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet generatedKeys = null;
        try {
            conn = H2RepositoryFactory.getConnection();
            stmt = conn.prepareStatement(SQL_CREATE_ACC);
            stmt.setString(1, account.getUsername());
            stmt.setBigDecimal(2, account.getBalance());
            stmt.setString(3, account.getCurrencyCode());
            int affectedRows = stmt.executeUpdate();
            if (affectedRows == 0) {
                throw new CustomException("Account Cannot be created");
            }
            generatedKeys = stmt.getGeneratedKeys();
            if (generatedKeys.next()) {
                return generatedKeys.getLong(1);
            } else {
                throw new CustomException("Account Cannot be created");
            }
        } catch (SQLException e) {
            throw new CustomException("Cannot create account");
        } finally {
            DbUtils.closeQuietly(conn, stmt, generatedKeys);
        }
    }

    public int deleteAccountById(long accountId) throws CustomException {
        Connection conn = null;
        PreparedStatement stmt = null;
        try {
            conn = H2RepositoryFactory.getConnection();
            stmt = conn.prepareStatement(SQL_DELETE_ACC_BY_ID);
            stmt.setLong(1, accountId);
            return stmt.executeUpdate();
        } catch (SQLException e) {
            throw new CustomException("Cannot delete account:  " + accountId);
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(stmt);
        }
    }

    public int updateAccountBalance(long accountId, BigDecimal deltaAmount) throws CustomException {
        Connection conn = null;
        PreparedStatement lockStmt = null;
        PreparedStatement updateStmt = null;
        ResultSet rs = null;
        Account targetAccount = null;
        int updateCount = -1;
        try {
            conn = H2RepositoryFactory.getConnection();
            conn.setAutoCommit(false);
            lockStmt = conn.prepareStatement(SQL_LOCK_ACC_BY_ID);
            lockStmt.setLong(1, accountId);
            rs = lockStmt.executeQuery();
            if (rs.next()) {
                targetAccount = new Account(rs.getLong("id"), rs.getString("username"),
                        rs.getBigDecimal("balance"), rs.getString("currencyCode"));
            }
            if (targetAccount == null) {
                throw new CustomException("Account lock failed for account: " + accountId);
            }
            BigDecimal balance = targetAccount.getBalance().add(deltaAmount);
            if (balance.compareTo(MoneyEnum.zeroAmount) < 0) {
                throw new CustomException("Insufficient balance for account: " + accountId);
            }

            updateStmt = conn.prepareStatement(SQL_UPDATE_ACC_BALANCE);
            updateStmt.setBigDecimal(1, balance);
            updateStmt.setLong(2, accountId);
            updateCount = updateStmt.executeUpdate();
            conn.commit();
            return updateCount;
        } catch (SQLException se) {
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException re) {
                throw new CustomException("Transaction rollback failed");
            }
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(lockStmt);
            DbUtils.closeQuietly(updateStmt);
        }
        return updateCount;
    }

    public int transferAccountBalance(Transaction transaction) throws CustomException {
        int result = -1;
        Connection conn = null;
        PreparedStatement lockStmt = null;
        PreparedStatement updateStmt = null;
        ResultSet rs = null;
        Account fromAccount = null;
        Account toAccount = null;

        try {
            conn = H2RepositoryFactory.getConnection();
            conn.setAutoCommit(false);
            lockStmt = conn.prepareStatement(SQL_LOCK_ACC_BY_ID);
            lockStmt.setLong(1, transaction.getFromAccountId());
            rs = lockStmt.executeQuery();
            if (rs.next()) {
                fromAccount = new Account(rs.getLong("id"), rs.getString("username"),
                        rs.getBigDecimal("balance"), rs.getString("currencyCode"));
            }
            lockStmt = conn.prepareStatement(SQL_LOCK_ACC_BY_ID);
            lockStmt.setLong(1, transaction.getToAccountId());
            rs = lockStmt.executeQuery();
            if (rs.next()) {
                toAccount = new Account(rs.getLong("id"), rs.getString("username"),
                        rs.getBigDecimal("balance"), rs.getString("currencyCode"));
            }

            if (fromAccount == null || toAccount == null) {
                throw new CustomException("Both fromAccount and toAccount are null");
            }

            if (!fromAccount.getCurrencyCode().equals(transaction.getCurrencyCode())) {
                throw new CustomException("Mismatch currencyCodes");
            }

            if (!fromAccount.getCurrencyCode().equals(toAccount.getCurrencyCode())) {
                throw new CustomException("Mismatch currencyCodes between accounts");
            }

            BigDecimal fromAccountLeftOver = fromAccount.getBalance().subtract(transaction.getAmount());
            if (fromAccountLeftOver.compareTo(MoneyEnum.zeroAmount) < 0) {
                throw new CustomException("Insufficient balance for fromAccount ");
            }
            updateStmt = conn.prepareStatement(SQL_UPDATE_ACC_BALANCE);
            updateStmt.setBigDecimal(1, fromAccountLeftOver);
            updateStmt.setLong(2, transaction.getFromAccountId());
            updateStmt.addBatch();
            updateStmt.setBigDecimal(1, toAccount.getBalance().add(transaction.getAmount()));
            updateStmt.setLong(2, transaction.getToAccountId());
            updateStmt.addBatch();
            int[] rowsUpdated = updateStmt.executeBatch();
            result = rowsUpdated[0] + rowsUpdated[1];
            conn.commit();
        } catch (SQLException se) {
            log.error("Transaction failed.");
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException re) {
                throw new CustomException("Transaction rollback failed");
            }
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(lockStmt);
            DbUtils.closeQuietly(updateStmt);
        }
        return result;
    }

}
