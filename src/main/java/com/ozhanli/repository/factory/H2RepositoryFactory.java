package com.ozhanli.repository.factory;

import com.ozhanli.repository.AccountRepository;
import com.ozhanli.repository.AccountRepositoryImpl;
import com.ozhanli.repository.UserRepository;
import com.ozhanli.repository.UserRepositoryImpl;
import com.ozhanli.utils.Utils;
import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;
import org.h2.tools.RunScript;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class H2RepositoryFactory extends RepositoryFactory {
    private static final String HD_DRIVER = Utils.getStringProperty("h2_driver");
    private static final String H2_CONNECTION_URL = Utils.getStringProperty("h2_connection_url");
    private static final String H2_USER = Utils.getStringProperty("h2_user");
    private static final String H2_PASSWORD = Utils.getStringProperty("h2_password");
    private static Logger log = Logger.getLogger(H2RepositoryFactory.class);

    private final UserRepositoryImpl userDAO = new UserRepositoryImpl();
    private final AccountRepositoryImpl accountDAO = new AccountRepositoryImpl();

    H2RepositoryFactory() {
        DbUtils.loadDriver(HD_DRIVER);
    }

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(H2_CONNECTION_URL, H2_USER, H2_PASSWORD);

    }

    public UserRepository getUserRepository() {
        return userDAO;
    }

    public AccountRepository getAccountRepository() {
        return accountDAO;
    }

    @Override
    public void populateDb() {
        Connection conn = null;
        try {
            conn = H2RepositoryFactory.getConnection();
            RunScript.execute(conn, new FileReader("src/test/resources/import.sql"));
        } catch (SQLException e) {
            log.error("Cannot populate db");
            throw new RuntimeException(e);
        } catch (FileNotFoundException e) {
            log.error("import.sql file not found.");
            throw new RuntimeException(e);
        } finally {
            DbUtils.closeQuietly(conn);
        }
    }

}
