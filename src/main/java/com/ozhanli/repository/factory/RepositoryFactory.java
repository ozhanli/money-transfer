package com.ozhanli.repository.factory;

import com.ozhanli.repository.AccountRepository;
import com.ozhanli.repository.UserRepository;

public abstract class RepositoryFactory {

    public static final int H2 = 1;

    public abstract UserRepository getUserRepository();

    public abstract AccountRepository getAccountRepository();

    public abstract void populateDb();

    public static RepositoryFactory getRepositoryFactory(int factoryCode) {
        switch (factoryCode) {
            case H2:
                return new H2RepositoryFactory();
            default:
                return new H2RepositoryFactory();
        }
    }
}
