package com.ozhanli;

import com.ozhanli.controller.AccountController;
import com.ozhanli.controller.TransactionController;
import com.ozhanli.controller.UserController;
import com.ozhanli.repository.factory.RepositoryFactory;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

public class Application {

    public static void main(String[] args) throws Exception {
        RepositoryFactory h2RepositoryFactory = RepositoryFactory.getRepositoryFactory(RepositoryFactory.H2);
        h2RepositoryFactory.populateDb();


        /*
        TODO: Guice DI implementation
        Injector injector = Guice.createInjector(new InjectionModule());

        AccountService accountService = injector.getInstance(AccountService.class);
        UserService userService = injector.getInstance(UserService.class);
        TransactionService transactionService = injector.getInstance(TransactionService.class);
        */

        /*
        TODO: AOP can be used for logging, Logging is not main concern for now
         */

        startService();
    }

    private static void startService() throws Exception {
        Server server = new Server(8080);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
        ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/*");
        servletHolder.setInitParameter("jersey.config.server.provider.classnames",
                String.format("%s,%s,%s",
                        AccountController.class.getCanonicalName(),
                        UserController.class.getCanonicalName(),
                        TransactionController.class.getCanonicalName()));

        try {
            server.start();
            server.join();
        } finally {
            server.destroy();
        }
    }

}