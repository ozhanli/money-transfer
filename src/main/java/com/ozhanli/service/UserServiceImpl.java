package com.ozhanli.service;

import com.ozhanli.exception.CustomException;
import com.ozhanli.model.User;
import com.ozhanli.model.base.BaseResponse;
import com.ozhanli.model.base.GenericResponse;
import com.ozhanli.repository.factory.RepositoryFactory;
import org.apache.log4j.Logger;

import javax.inject.Singleton;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

@Singleton
public class UserServiceImpl implements UserService {

    private final RepositoryFactory repositoryFactory = RepositoryFactory.getRepositoryFactory(RepositoryFactory.H2);
    private static Logger log = Logger.getLogger(UserServiceImpl.class);

    @Override
    public BaseResponse getByUsername(String username) throws CustomException {
        if (log.isDebugEnabled())
            log.debug("Request Received for get User by Name " + username);
        final User user = repositoryFactory.getUserRepository().getByUsername(username)
                .orElseThrow(() -> new WebApplicationException("User Not Found", Response.Status.NOT_FOUND));
        return new GenericResponse<>("Operation success", user);
    }

    @Override
    public List<User> getAllUsers() throws CustomException {
        return repositoryFactory.getUserRepository().getAllUsers();
    }

    @Override
    public BaseResponse create(User user) throws CustomException {
        Optional<User> userOptional = repositoryFactory.getUserRepository().getByUsername(user.getUsername());
        if (userOptional.isPresent()) {
            throw new WebApplicationException("User name already exist", Response.Status.BAD_REQUEST);
        }
        final long userId = repositoryFactory.getUserRepository().insert(user);
        User created = repositoryFactory.getUserRepository().getById(userId)
                .orElseThrow(() -> new WebApplicationException("User not found", Response.Status.BAD_REQUEST));
        return new GenericResponse<>("Operation success", created);
    }

    @Override
    public BaseResponse update(Long userId, User user) throws CustomException {
        final int updateCount = repositoryFactory.getUserRepository().update(userId, user);
        if (updateCount != 1) {
            throw new WebApplicationException("User id not found.", Response.Status.NOT_FOUND);
        }
        return new GenericResponse<>("Operation success", null);
    }

    @Override
    public BaseResponse delete(Long userId) throws CustomException {
        int deleteCount = repositoryFactory.getUserRepository().delete(userId);
        if (deleteCount != 1) {
            throw new WebApplicationException("User id not found.", Response.Status.NOT_FOUND);
        }
        return new GenericResponse<>("Operation success", null);
    }


}
