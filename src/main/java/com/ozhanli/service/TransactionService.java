package com.ozhanli.service;

import com.ozhanli.exception.CustomException;
import com.ozhanli.model.Transaction;
import com.ozhanli.model.base.BaseResponse;

import javax.ws.rs.core.Response;

public interface TransactionService {

    BaseResponse transfer(Transaction transaction) throws CustomException;

}
