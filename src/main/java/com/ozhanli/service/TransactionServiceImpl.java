package com.ozhanli.service;

import com.ozhanli.enums.MoneyEnum;
import com.ozhanli.exception.CustomException;
import com.ozhanli.model.Transaction;
import com.ozhanli.model.base.BaseResponse;
import com.ozhanli.model.base.GenericResponse;
import com.ozhanli.repository.factory.RepositoryFactory;

import javax.inject.Singleton;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

@Singleton
public class TransactionServiceImpl implements TransactionService {

    private final RepositoryFactory repositoryFactory = RepositoryFactory.getRepositoryFactory(RepositoryFactory.H2);

    public BaseResponse transfer(Transaction transaction) throws CustomException {
        String currency = transaction.getCurrencyCode();

        if (!MoneyEnum.INSTANCE.validateCurrencyCode(currency))
            throw new WebApplicationException("Invalid currency code ", Response.Status.BAD_REQUEST);

        int updateCount = repositoryFactory.getAccountRepository().transferAccountBalance(transaction);
        if (updateCount == 2)
            return new GenericResponse<>("Operation success", Response.Status.OK);

        throw new WebApplicationException("Operation failed", Response.Status.BAD_REQUEST);
    }

}
