package com.ozhanli.service;

import com.ozhanli.enums.MoneyEnum;
import com.ozhanli.exception.CustomException;
import com.ozhanli.model.Account;
import com.ozhanli.model.base.BaseResponse;
import com.ozhanli.model.base.GenericResponse;
import com.ozhanli.repository.AccountRepository;
import com.ozhanli.repository.factory.RepositoryFactory;
import org.apache.log4j.Logger;

import javax.inject.Singleton;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Singleton
public class AccountServiceImpl implements AccountService {

    private final RepositoryFactory repositoryFactory = RepositoryFactory.getRepositoryFactory(RepositoryFactory.H2);
    private static Logger log = Logger.getLogger(AccountServiceImpl.class);

    public BaseResponse getAccounts() throws CustomException {
        List<Account> accounts = repositoryFactory.getAccountRepository().getAllAccounts();
        return new GenericResponse<>("Operation success", accounts);
    }

    public BaseResponse getAccountById(Long accountId) throws CustomException {
        return new GenericResponse<>("Operation success", findAccountById(accountId));
    }

    public BaseResponse getAccountBalanceById(Long accountId) throws CustomException {
        Account account = findAccountById(accountId);
        return new GenericResponse<>("Operation success", account.getBalance());
    }

    public BaseResponse createAccount(Account account) throws CustomException {
        final long accountId = repositoryFactory.getAccountRepository().createAccount(account);
        return new GenericResponse<>("Operation success", findAccountById(accountId));
    }

    public BaseResponse depositToAccount(Long accountId, BigDecimal amount) throws CustomException {
        if (amount.compareTo(MoneyEnum.zeroAmount) <= 0)
            throw new WebApplicationException("Invalid amount", Response.Status.BAD_REQUEST);
        AccountRepository accountRepository = repositoryFactory.getAccountRepository();
        accountRepository.updateAccountBalance(accountId, amount.setScale(4, RoundingMode.HALF_EVEN));
        return new GenericResponse<>("Operation success", findAccountById(accountId));
    }

    public BaseResponse withdrawFromAccount(Long accountId, BigDecimal amount) throws CustomException {
        if (amount.compareTo(MoneyEnum.zeroAmount) <= 0) {
            throw new WebApplicationException("Invalid amount", Response.Status.BAD_REQUEST);
        }
        amount = amount.negate().setScale(4, RoundingMode.HALF_EVEN);
        if (log.isDebugEnabled())
            log.debug("Withdrew " + amount + "from account " + accountId);
        repositoryFactory.getAccountRepository().updateAccountBalance(accountId, amount);
        return new GenericResponse<>("Operation success", findAccountById(accountId));
    }

    public BaseResponse deleteAccountById(Long accountId) throws CustomException {
        int deleteCount = repositoryFactory.getAccountRepository().deleteAccountById(accountId);
        if (deleteCount != 1)
            throw new WebApplicationException("Account not found", Response.Status.NOT_FOUND);
        return new GenericResponse<>("Operation success", null);
    }

    private Account findAccountById(Long accountId) throws CustomException {
        return repositoryFactory.getAccountRepository().getAccountById(accountId)
                .orElseThrow(() -> new WebApplicationException("Account not found", Response.Status.NOT_FOUND));
    }

}
