package com.ozhanli.service.factory;

import com.ozhanli.service.AccountService;
import com.ozhanli.service.TransactionService;
import com.ozhanli.service.UserService;

public abstract class ServiceFactory {

    public abstract AccountService getAccountService();

    public abstract TransactionService getTransactionService();

    public abstract UserService getUserService();

    public static ServiceFactory getServiceFactory(int factoryCode) {
        switch (factoryCode) {
            default:
                return new DefaultServiceFactory();
        }
    }
}
