package com.ozhanli.service.factory;

import com.ozhanli.service.*;

public class DefaultServiceFactory extends ServiceFactory {

    private final AccountServiceImpl accountService = new AccountServiceImpl();
    private final TransactionServiceImpl transactionService = new TransactionServiceImpl();
    private final UserServiceImpl userService = new UserServiceImpl();

    DefaultServiceFactory() {
    }

    @Override
    public AccountService getAccountService() {
        return accountService;
    }

    @Override
    public TransactionService getTransactionService() {
        return transactionService;
    }

    @Override
    public UserService getUserService() {
        return userService;
    }
}
