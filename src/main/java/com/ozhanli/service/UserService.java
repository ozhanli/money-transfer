package com.ozhanli.service;

import com.ozhanli.exception.CustomException;
import com.ozhanli.model.User;
import com.ozhanli.model.base.BaseResponse;

import javax.ws.rs.core.Response;
import java.util.List;


public interface UserService {

    BaseResponse getByUsername(String username) throws CustomException;

    List<User> getAllUsers() throws CustomException;

    BaseResponse create(User user) throws CustomException;

    BaseResponse update(Long userId, User user) throws CustomException;

    BaseResponse delete(Long userId) throws CustomException;

}
