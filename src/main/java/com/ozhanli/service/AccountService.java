package com.ozhanli.service;

import com.ozhanli.exception.CustomException;
import com.ozhanli.model.Account;
import com.ozhanli.model.base.BaseResponse;

import java.math.BigDecimal;


public interface AccountService {

    BaseResponse getAccounts() throws CustomException;

    BaseResponse getAccountById(Long accountId) throws CustomException;

    BaseResponse getAccountBalanceById(Long accountId) throws CustomException;

    BaseResponse createAccount(Account account) throws CustomException;

    BaseResponse depositToAccount(Long accountId, BigDecimal amount) throws CustomException;

    BaseResponse withdrawFromAccount(Long accountId, BigDecimal amount) throws CustomException;

    BaseResponse deleteAccountById(Long accountId) throws CustomException;

}
