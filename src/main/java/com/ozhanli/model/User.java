package com.ozhanli.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class User {

    @JsonIgnore
    private long id;

    @JsonProperty(required = true)
    private String username;

    @JsonProperty(required = true)
    private String email;

    public User(String username, String email) {
        this.username = username;
        this.email = email;
    }

    public User(long id, String username, String email) {
        this.id = id;
        this.username = username;
        this.email = email;
    }
}
