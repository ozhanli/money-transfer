package com.ozhanli.model.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GenericResponse<Content> extends BaseResponse {
    Content response;

    public GenericResponse(String message, Content response) {
        super(message);
        this.response = response;
    }

}
