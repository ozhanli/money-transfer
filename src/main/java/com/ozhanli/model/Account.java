package com.ozhanli.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class Account {

    @JsonIgnore
    private long id;

    @JsonProperty(required = true)
    private String username;

    @JsonProperty(required = true)
    private BigDecimal balance;

    @JsonProperty(required = true)
    private String currencyCode;

    public Account(String username, BigDecimal balance, String currencyCode) {
        this.username = username;
        this.balance = balance;
        this.currencyCode = currencyCode;
    }

    public Account(long id, String username, BigDecimal balance, String currencyCode) {
        this.id = id;
        this.username = username;
        this.balance = balance;
        this.currencyCode = currencyCode;
    }
}
