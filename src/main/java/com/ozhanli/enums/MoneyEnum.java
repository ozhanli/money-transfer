package com.ozhanli.enums;

import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;

public enum MoneyEnum {

    INSTANCE;

    static Logger log = Logger.getLogger(MoneyEnum.class);

    public static final BigDecimal zeroAmount = new BigDecimal(0).setScale(4, RoundingMode.HALF_EVEN);

    public boolean validateCurrencyCode(String inputCcyCode) {
        try {
            Currency instance = Currency.getInstance(inputCcyCode);
            return instance.getCurrencyCode().equals(inputCcyCode);
        } catch (Exception e) {
            log.error("Currency validation failed.");
        }
        return false;
    }

}
