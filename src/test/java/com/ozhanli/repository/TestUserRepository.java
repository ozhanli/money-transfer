package com.ozhanli.repository;

import com.ozhanli.exception.CustomException;
import com.ozhanli.model.User;
import com.ozhanli.repository.factory.RepositoryFactory;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.*;

public class TestUserRepository {

    private static Logger log = Logger.getLogger(TestUserRepository.class);

    private static final RepositoryFactory H_2_REPOSITORY_FACTORY = RepositoryFactory.getRepositoryFactory(RepositoryFactory.H2);

    @BeforeClass
    public static void setup() {
        log.debug("setting up test database and sample data....");
        H_2_REPOSITORY_FACTORY.populateDb();
    }

    @Test
    public void getAllUsersTest() throws CustomException {
        List<User> users = H_2_REPOSITORY_FACTORY.getUserRepository().getAllUsers();
        assertTrue(users.size() > 1);
    }

    @Test
    public void getUserByIdTest() throws CustomException {
        Optional<User> userOptional = H_2_REPOSITORY_FACTORY.getUserRepository().getById(2L);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            assertEquals("asaf", user.getUsername());
        }
    }

    @Test
    public void nonExistingUserByIdTest() throws CustomException {
        Optional<User> userOptional = H_2_REPOSITORY_FACTORY.getUserRepository().getById(500L);
        User user = null;
        if (userOptional.isPresent()) {
            user = userOptional.get();
        }
        assertNull(user);
    }

    @Test
    public void nonExistingUserByUsernameTest() throws CustomException {
        Optional<User> userOptional = H_2_REPOSITORY_FACTORY.getUserRepository().getByUsername("xyz");
        User user = null;
        if (userOptional.isPresent()) {
            user = userOptional.get();
        }
        assertNull(user);
    }

    @Test
    public void createUserTest() throws CustomException {
        User create = new User("halil", "hozhanli@gmail.com");
        long id = H_2_REPOSITORY_FACTORY.getUserRepository().insert(create);
        Optional<User> createdOptional = H_2_REPOSITORY_FACTORY.getUserRepository().getById(id);
        if (createdOptional.isPresent()) {
            User created = createdOptional.get();
            assertEquals("halil", created.getUsername());
            assertEquals("hozhanli@gmail.com", created.getEmail());
        }
    }

    @Test
    public void updateUserTest() throws CustomException {
        User user = new User(1L, "yasemin", "yasemin@gmail.com");
        int rowCount = H_2_REPOSITORY_FACTORY.getUserRepository().update(1L, user);
        assertEquals(1, rowCount);
        Optional<User> userByIdOptional = H_2_REPOSITORY_FACTORY.getUserRepository().getById(1L);
        if (userByIdOptional.isPresent()) {
            User userById = userByIdOptional.get();
            assertEquals("omer@gmail.com", userById.getEmail());
        }
    }

    @Test
    public void updateNonExistingUserTest() throws CustomException {
        User create = new User(500L, "test2", "test2@gmail.com");
        int rowCount = H_2_REPOSITORY_FACTORY.getUserRepository().update(500L, create);
        assertEquals(0, rowCount);
    }

    @Test
    public void deleteUserByIdTest() throws CustomException {
        int rowCount = H_2_REPOSITORY_FACTORY.getUserRepository().delete(1L);
        assertEquals(1, rowCount);
        Optional<User> userOptional = H_2_REPOSITORY_FACTORY.getUserRepository().getById(1L);
        User user = null;
        if (userOptional.isPresent()) {
            user = userOptional.get();
        }
        assertNull(user);
    }

    @Test
    public void deleteNonExistingUserByIdTest() throws CustomException {
        int rowCount = H_2_REPOSITORY_FACTORY.getUserRepository().delete(500L);
        assertEquals(0, rowCount);
    }

}
