package com.ozhanli.repository;

import com.ozhanli.exception.CustomException;
import com.ozhanli.model.Account;
import com.ozhanli.model.Transaction;
import com.ozhanli.repository.factory.RepositoryFactory;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.concurrent.CountDownLatch;

import static junit.framework.TestCase.assertEquals;

public class TestAccountBalance {

    private static Logger log = Logger.getLogger(TestAccountRepository.class);
    private static final RepositoryFactory H_2_REPOSITORY_FACTORY = RepositoryFactory.getRepositoryFactory(RepositoryFactory.H2);
    private static final int THREADS_COUNT = 10;

    @BeforeClass
    public static void setup() {
        H_2_REPOSITORY_FACTORY.populateDb();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void singleThreadTransferTest() throws CustomException {
        final AccountRepository accountRepository = H_2_REPOSITORY_FACTORY.getAccountRepository();
        BigDecimal transferAmount = new BigDecimal(100).setScale(4, RoundingMode.HALF_EVEN);
        Transaction transaction = new Transaction("USD", transferAmount, 1L, 2L);
        accountRepository.transferAccountBalance(transaction);
        Account from = accountRepository.getAccountById(1L)
                .orElseThrow(() -> new CustomException("Invalid account"));
        Account to = accountRepository.getAccountById(2L)
                .orElseThrow(() -> new CustomException("Invalid account"));

        assertEquals(new BigDecimal(50), from.getBalance());
        assertEquals(new BigDecimal(250), to.getBalance());
    }

    @Test
    public void multiThreadTransferTest() throws InterruptedException, CustomException {
        final AccountRepository accountRepository = H_2_REPOSITORY_FACTORY.getAccountRepository();
        final CountDownLatch latch = new CountDownLatch(THREADS_COUNT);
        for (int i = 0; i < THREADS_COUNT; i++) {
            new Thread(() -> {
                try {
                    Transaction transaction = new Transaction("USD",
                            new BigDecimal(50).setScale(4, RoundingMode.HALF_EVEN), 1L, 2L);
                    accountRepository.transferAccountBalance(transaction);
                } catch (Exception e) {
                    log.error("Error occurred during transfer ", e);
                } finally {
                    latch.countDown();
                }
            }).start();
        }

        latch.await();
        Account from = accountRepository.getAccountById(1L)
                .orElseThrow(() -> new CustomException("Invalid account"));
        Account to = accountRepository.getAccountById(2L)
                .orElseThrow(() -> new CustomException("Invalid account"));

        assertEquals(from.getBalance(), new BigDecimal(0).setScale(4, RoundingMode.HALF_EVEN));
        assertEquals(to.getBalance(), new BigDecimal(300).setScale(4, RoundingMode.HALF_EVEN));
    }

}
