package com.ozhanli.repository;

import com.ozhanli.exception.CustomException;
import com.ozhanli.model.Account;
import com.ozhanli.repository.factory.RepositoryFactory;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.*;

public class TestAccountRepository {

    private static final RepositoryFactory H_2_REPOSITORY_FACTORY = RepositoryFactory.getRepositoryFactory(RepositoryFactory.H2);

    @BeforeClass
    public static void setup() {
        H_2_REPOSITORY_FACTORY.populateDb();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void getAllAccountsTest() throws CustomException {
        List<Account> allAccounts = H_2_REPOSITORY_FACTORY.getAccountRepository().getAllAccounts();
        assertTrue(allAccounts.size() > 1);
    }

    @Test
    public void getAccountByIdTest() throws CustomException {
        Optional<Account> accountOptional = H_2_REPOSITORY_FACTORY.getAccountRepository().getAccountById(1L);
        if (accountOptional.isPresent()) {
            Account account = accountOptional.get();
            assertEquals("asaf", account.getUsername());
        }
    }

    @Test
    public void getNonExistingAccountByIdTest() throws CustomException {
        Optional<Account> accountOptional = H_2_REPOSITORY_FACTORY.getAccountRepository().getAccountById(500L);
        Account account = null;
        if (accountOptional.isPresent())
            account = accountOptional.get();
        assertNull(account);
    }

    @Test
    public void testCreateAccountTest() throws CustomException {
        BigDecimal balance = new BigDecimal(10).setScale(4, RoundingMode.HALF_EVEN);
        Account account = new Account("asaf", balance, "TRY");
        Long accountId = H_2_REPOSITORY_FACTORY.getAccountRepository().createAccount(account);
        Optional<Account> createdAccountOptional = H_2_REPOSITORY_FACTORY.getAccountRepository().getAccountById(accountId);
        Account createdAccount = null;
        if (createdAccountOptional.isPresent()) {
            createdAccount = createdAccountOptional.get();
            assertEquals("TRY", createdAccount.getUsername());
            assertEquals("CNY", createdAccount.getCurrencyCode());
            assertEquals(createdAccount.getBalance(), balance);
        }
    }

}