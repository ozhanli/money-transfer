DROP TABLE IF EXISTS User;

CREATE TABLE User (
  id       LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
  username VARCHAR(30) NOT NULL,
  email    VARCHAR(30) NOT NULL
);

CREATE UNIQUE INDEX idx_ue
  on User (username, email);

INSERT INTO User (username, email)
VALUES ('omer', 'omer@gmail.com');
INSERT INTO User (username, email)
VALUES ('asaf', 'asaf@gmail.com');
INSERT INTO User (username, email)
VALUES ('ozhanli', 'ozhanli@gmail.com');

DROP TABLE IF EXISTS Account;

CREATE TABLE Account (
  id           LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
  username     VARCHAR(30),
  balance      DECIMAL(19, 4),
  currencyCode VARCHAR(30)
);

CREATE UNIQUE INDEX idx_acc
  on Account (UserName, CurrencyCode);

INSERT INTO Account (username, balance, currencyCode)
VALUES ('omer', 100.0000, 'USD');
INSERT INTO Account (username, balance, currencyCode)
VALUES ('asaf', 200.0000, 'USD');
INSERT INTO Account (username, balance, currencyCode)
VALUES ('omer', 500.0000, 'EUR');
INSERT INTO Account (username, balance, currencyCode)
VALUES ('asaf', 500.0000, 'EUR');
INSERT INTO Account (username, balance, currencyCode)
VALUES ('omer', 500.0000, 'GBP');
INSERT INTO Account (username, balance, currencyCode)
VALUES ('asaf', 500.0000, 'GBP');
