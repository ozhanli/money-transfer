# Money transfer Rest API

Money transfer API

# Services

| HTTP        | ENDPOINT   | EXPLANATION  |
| ----------- | ------     | ------   |
| GET | /accounts | Gets all defined accounts |
| GET | /accounts/{accountId}  | Gets account by specified id |
| POST | /accounts | Creates account |
| PUT | /accounts/{accountId}/deposit/{amount}  | Deposit money to account |
| PUT | /accounts/{accountId}/withdraw/{amount}  | Withdraw money from account |
| DELETE | /accounts/{accountId}  | Deletes account specified by id |
| POST | /transactions | Perform transaction between 2 accounts |
| GET | /users | Gets all defined users |
| GET | /users/{username} | Gets user specified by id |
| POST | /users | Creates user | 
| PUT | /users/{userId} | Updates user by id | 
| DELETE | /users/{userId} | Deletes account specified by id |

### How to run

```sh
mvn exec:java
```